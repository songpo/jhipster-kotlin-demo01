/**
 * View Models used by Spring MVC REST controllers.
 */
package com.liusongpo.jhipster02.web.rest.vm;
