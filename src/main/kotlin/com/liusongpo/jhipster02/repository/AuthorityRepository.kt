package com.liusongpo.jhipster02.repository

import com.liusongpo.jhipster02.domain.Authority

import org.springframework.data.jpa.repository.JpaRepository

/**
 * Spring Data JPA repository for the Authority entity.
 */
interface AuthorityRepository : JpaRepository<Authority, String>
