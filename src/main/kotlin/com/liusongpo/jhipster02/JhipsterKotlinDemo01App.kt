package com.liusongpo.jhipster02
import com.liusongpo.jhipster02.config.ApplicationProperties
import com.liusongpo.jhipster02.config.DefaultProfileUtil

import io.github.jhipster.config.JHipsterConstants

import org.slf4j.LoggerFactory
import org.springframework.boot.runApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.core.env.Environment

import javax.annotation.PostConstruct
import java.net.InetAddress
import java.net.UnknownHostException

@SpringBootApplication
@EnableConfigurationProperties(LiquibaseProperties::class, ApplicationProperties::class)
@EnableDiscoveryClient
class JhipsterKotlinDemo01App(private val env: Environment) {

    private val log = LoggerFactory.getLogger(JhipsterKotlinDemo01App::class.java)

    /**
     * Initializes jhipster_kotlin_demo01.
     * <p>
     * Spring profiles can be configured with a program arguments --spring.profiles.active=your-active-profile
     * <p>
     * You can find more information on how profiles work with JHipster on <a href="http://www.jhipster.tech/profiles/">http://www.jhipster.tech/profiles/</a>.
     */
    @PostConstruct
    fun initApplication() {
        val activeProfiles = env.activeProfiles
        if (activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_DEVELOPMENT) && activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_PRODUCTION)) {
            log.error("You have misconfigured your application! It should not run " + "with both the 'dev' and 'prod' profiles at the same time.")
        }
        if (activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_DEVELOPMENT) && activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_CLOUD)) {
            log.error("You have misconfigured your application! It should not " + "run with both the 'dev' and 'cloud' profiles at the same time.")
        }
    }

    companion object {
        /**
        * Main method, used to run the application.
        *
        * @param args the command line arguments
        * @throws UnknownHostException if the local host name could not be resolved into an address
        */
        @Throws(UnknownHostException::class)
        @JvmStatic
        fun main(args: Array<String>) {
            val log = LoggerFactory.getLogger(JhipsterKotlinDemo01App::class.java)

            val env = runApplication<JhipsterKotlinDemo01App> {
                setDefaultProperties(DefaultProfileUtil.addDefaultProfile())
            }.environment

            var protocol = "http"
            if (env.getProperty("server.ssl.key-store") != null) {
                protocol = "https"
            }
            log.info("\n----------------------------------------------------------\n\t" +
                "Application '{}' is running! Access URLs:\n\t" +
                "Local: \t\t{}://localhost:{}\n\t" +
                "External: \t{}://{}:{}\n\t" +
                "Profile(s): \t{}\n----------------------------------------------------------",
                env.getProperty("spring.application.name"),
                protocol,
                env.getProperty("server.port"),
                protocol,
                InetAddress.getLocalHost().hostAddress,
                env.getProperty("server.port"),
                env.activeProfiles)

        }
    }
}
